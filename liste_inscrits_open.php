<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <LINK rel="stylesheet" type="text/css" href="style.css">
    <title>Liste inscrits Open 24h</title>
    <style>
        table {
            border-collapse: collapse;
            width: 100%;
        }
        th, td {
            border: 1px solid #dddddd;
            text-align: left;
            padding: 8px;
        }
        th {
            background-color: #f2f2f2;
        }
    </style>
</head>
<body>

<table id="csvTable">
    <thead>
        <tr>
            <th>Dossard</th>
            <th>Nom</th>
            <th>Prénom</th>
        </tr>
    </thead>
    <tbody>
        <?php
        // Chemin vers le fichier CSV
        $csvFilePath = 'liste_inscrits_open.csv';

        // Lire le contenu du fichier CSV
        $csvFile = fopen($csvFilePath, 'r');

        // Parcourir les lignes du fichier CSV
        while (($data = fgetcsv($csvFile, 0, ",")) !== FALSE) {
            // Afficher les données dans une ligne de tableau HTML
            echo "<tr>";
	    $line = 0;
            foreach ($data as $value) {
		if ($line < 3)
                echo "<td>" . htmlspecialchars($value) . "</td>"; // Utilisation de htmlspecialchars pour éviter les failles XSS
		$line++;
            }
            echo "</tr>";
        }

        // Fermer le fichier CSV
        fclose($csvFile);
        ?>
    </tbody>
</table>

</body>
</html>
