# Pointage des 100kms de Steenwerck

Système de pointage pour la course des 100km à pied de Steenwerck, basé sur une solution full web et BDD mariadb

## Getting Started

Ces instructions permettre de définir le projet ainsi que d'en faire une copie.

**Table of content**

- [Structure](#structure)
- [Installation serveur](#installation-serveur)
- [Installation PC pointage](#installation-pc-pointage)
- [Tester](#tester)
- [Jour J](#jour-j)
- [Send emails](#send-emails)
- [Fonctionnalités](#fonctionnalités)
- [Bugs connus](#bugs-conus)


## Structure

Le système de pointage est composé d'un serveur central et de 5 PC utilisés par les pointeurs. Chaque PC dispose d'une base de donnée mariadb qui est ensuite répliquée vers le serveur central.

![100kms-Pointage-diagram.drawio](/uploads/bce201da702bb5ce58c14d359edbd2e9/100kms-Pointage-diagram.drawio.png)

## Installation serveur
https://help.ovhcloud.com/csm/fr-vps-security-tips?id=kb_article_view&sysparm_article=KB0047708

### Déployer les softwares et configurations
```shell
apt install apache2
apt install mariadb-server
systemctl start mariadb.service
mysql_secure_installation
apt install php8.2 php8.2-{cli,mysql,imap,intl,apcu,cgi,bz2,zip,mbstring,gd,curl,xml,common,opcache,imagick} -y
apt install libapache2-mod-php libapache2-mod-php8.2 -y
a2enmod php8.2
apt install phpmyadmin
apt install git vim
vim /etc/mysql/mariadb.cnf
```
```
[mysqld]
slave_skip_errors=1032,1236
```
```shell
systemctl restart mysql
vim /etc/apache2/sites-available/100kmsteenwerck.conf
```
```Apache configuration
    <Location "/admin">
        AuthType Basic
        AuthName "Zone d'administration"
        AuthUserFile /var/www/html/admin/.htpasswd
        Require valid-user
    </Location>
```
```shell
htpasswd -nbB admin mon_mot_de_passe >> /var/www/html/admin/.htpasswd
a2ensite 100kmsteenwerck.conf
vim /etc/ssh/sshd_config
```
```SSH configuration
Port x
PermitRootLogin No
PasswordAuthentication no
X11Forwarding no
```
```shell
systemctl restart sshd
apt install wireguard
umask 077
wg genkey | tee privatekey | wg pubkey > publickey
wg genkey | tee clt6_privatekey | wg pubkey > clt6_publickey
vim /etc/wireguard/wg0.conf
```
```Wireguard configuration
[Interface]
PrivateKey = 
Address = x.x.x.x/24
ListenPort = x

[peer]
PublicKey = 
AllowedIPs = 10.x.x.x/32
Endpoint = x.x.x.x:x
PersistentKeepalive = 20

[peer]
...
```
```shell
systemctl enable wg-quick@wg0
wg-quick down wg0 ; wg-quick up wg0
cd /root ; git clone https://framagit.org/100km-steenwerck/pointage.git
cp /root/pointage/* /var/www/html/
cp -r /root/pointage/admin/ /var/www/html/
htpasswd -c /var/www/html/admin/.htpasswd admin
chown -R www-data:www-data /var/www/
chmod -R 644 /var/www/html/
chmox +x /var/www/html/admin/ 
touch /var/www/html/config.php
chmod 600 /var/www/html/config.php
vim /var/www/html/config.php  # set password
mysql -u root -p
```
### Déployer les BDD

```sql
// Creation des bases et tables
CREATE DATABASE coureurs;
CREATE DATABASE Pointage1;
CREATE DATABASE Pointage2;
CREATE DATABASE Pointage3;
CREATE DATABASE Pointage4;
CREATE DATABASE Pointage5;
CREATE TABLE coureurs.2025 (`Dossard` INT,`Nom` TEXT,`Prenom` TEXT,`Annee` YEAR(4),`Sexe` ENUM('H', 'F'),`Course` ENUM('Open', 'Course'));
CREATE TABLE Pointage1.2025 (`Dossard` INT,`Date` DATETIME);
CREATE TABLE Pointage2.2025 (`Dossard` INT,`Date` DATETIME);
CREATE TABLE Pointage3.2025 (`Dossard` INT,`Date` DATETIME);
CREATE TABLE Pointage4.2025 (`Dossard` INT,`Date` DATETIME);
CREATE TABLE Pointage5.2025 (`Dossard` INT,`Date` DATETIME);
```

### Déployer la réplication

```sql
// Démarrer la réplication en modifiant les valeurs de MASTER_LOG_FILE & MASTER_LOG_POS en fonction du retour de la commande "SHOW MASTER STATUS;" sur chaque master
SET @@default_master_connection='mariadb1';
CHANGE MASTER 'mariadb1' TO MASTER_HOST='192.168.142.51', MASTER_PORT=3306, MASTER_USER='slave', MASTER_PASSWORD='password', MASTER_LOG_FILE='mysql-bin.000008', MASTER_LOG_POS=997;
SET @@default_master_connection='mariadb2';
CHANGE MASTER 'mariadb2' TO MASTER_HOST='192.168.142.52', MASTER_PORT=3306, MASTER_USER='slave', MASTER_PASSWORD='password', MASTER_LOG_FILE='mysql-bin.000001', MASTER_LOG_POS=1248;
SET @@default_master_connection='mariadb3';
CHANGE MASTER 'mariadb3' TO MASTER_HOST='192.168.142.53', MASTER_PORT=3306, MASTER_USER='slave', MASTER_PASSWORD='password', MASTER_LOG_FILE='mysql-bin.000001', MASTER_LOG_POS=1248;
SET @@default_master_connection='mariadb4';
CHANGE MASTER 'mariadb4' TO MASTER_HOST='192.168.142.54', MASTER_PORT=3306, MASTER_USER='slave', MASTER_PASSWORD='password', MASTER_LOG_FILE='mysql-bin.000001', MASTER_LOG_POS=1248;
SET @@default_master_connection='mariadb5';
CHANGE MASTER 'mariadb5' TO MASTER_HOST='192.168.142.55', MASTER_PORT=3306, MASTER_USER='slave', MASTER_PASSWORD='password', MASTER_LOG_FILE='mysql-bin.000001', MASTER_LOG_POS=1248;
start replica;
```

### Déployer la liste des coureurs dans la table "coureurs.2024"
```sql
INSERT INTO `2024` (`Dossard`, `Nom`, `Prenom`, `Annee`, `Sexe`, `Steenwerck`, `Course`) VALUES ('998', 'Steenwerkois', 'Anne-Marie', '1978','F','1','Course');
```

## Installation PC pointage
```shell
apt-get update ; apt-get upgrade
apt install apache2 mariadb-server
systemctl start mariadb.service
mysql_secure_installation
apt install php8.1 php8.1-{cli,mysql,imap,intl,apcu,cgi,bz2,zip,mbstring,gd,curl,xml,common,opcache,imagick} -y
apt install libapache2-mod-php libapache2-mod-php8.1 -y
a2enmod php8.1
apt install phpmyadmin git vim
add-apt-repository ppa:wireguard/wireguard
apt-get install wireguard resolvconf
vim /etc/wireguard/wg0.conf
```
```configuration wireguard
[Interface]
Address = x.x.x.x:x
ListenPort = x
PrivateKey = 

[Peer]
PublicKey = 
Endpoint = x.x.x.x:x
AllowedIps = x.x.x.x:x
PersistentKeepAlive = 25
```
```shell
systemctl enable wg-quick@wg0
wg-quick down wg0 ; wg-quick up wg0
cd /root ; iptables -F; git clone https://framagit.org/100km-steenwerck/pointage.git ; iptables -A OUTPUT -p tcp --dport 53 -j DROP ; iptables -A OUTPUT -p udp --dport 53 -j DROP
cp /root/pointage/pointeur/* /var/www/html/
touch /var/www/html/config.php
chown -R www-data:www-data /var/www/html/
chmod -R 644 /var/www/html/
chmod +x /var/www/html/ 
chmod 600 /var/www/html/config.php
vim /var/www/html/config.php  # set password
```
```php
<?php
// Paramètres de connexion à la base de données
$servername = "localhost";
$username = "";
$password = "";
$db = "Pointage4";
$lieu = "La Blanche";
$table = "2024";
nbrDossard = "999";
?>
```
```shell
vim /etc/mysql/mariadb.cnf
```
```
[mysqld]
server-id = 4
bind-address = 0.0.0.0
log-bin = /var/log/mysql/mysql-bin.log
binlog-do-db = Pointage4
```
```shell
vim /etc/mysql/mariadb.conf.d/50-server.cnf
```
expire_logs_days        = 0
```shell
mkdir /var/log/mysql/
touch /var/log/mysql/mysql-bin.log
touch /var/log/mysql/mysql-error.log
chown mysql -Rf /var/log/mysql/
systemctl restart mysql
mysql -u root -p
```
```sql
CREATE DATABASE Pointage2;
CREATE TABLE Pointage2.2024 (`Dossard` INT,`Date` DATETIME);
SHOW MASTER STATUS
```

Modifier les passwords dans /phpmyadmin et ajouter les utilisateurs www-data (droits données) et slave (droit réplication slave)
Supprimer les droits sudoers et changer le passwd root

## Tester la réplication de bdd

Ajouter une valeur dans chacune des bdd satellites des PC et vérifier que c'est répliqué sur le master.

```sql
INSERT into `2024` VALUES ("1", "2024-03-14 13:15:12"); 
```

## Send emails
```bash
apt-get install msmtp
vim /etc/php/8.2/apache2/php.ini
```
sendmail_path = /usr/bin/msmtp -t

```bash
service apache2 reload
touch /var/log/msmtp
chmod 660 /var/log/msmtp
chown www-data:www-data /var/log/msmtp
touch /etc/msmtprc
chmod 666 /etc/msmtprc
vim /etc/msmtprc
```
account         cliss
auth            on
tls             on
tls_starttls    on
tls_certcheck   off
host            courriel.cliss21.com
port            465
from
user
password
logfile         /var/log/msmtp
account default : cliss


## Jour J
Dès que la liste des coureurs qui les associe avec leur numéro de dossard est disponible, il faut récupérer cette liste sous la forme csv suivante :
```
`Dossard`	`Nom`	`Prenom`	`Annee`	`Sexe`	`Steenwerck`	`Course`
24	GOMES	Audrey	2002	F	1	Open
```
Ensuite, pousser cette liste sur le serveur à l'emplacement : /home/debian/liste.csv
Et exécuter le script : ./Import_Liste.py
Il ne reste plus qu'à copier les requêtes SQL dans phpmyadmin, base de données "coureurs" et dans la table de l'année correspondante.

Le jour de la course, même si la synchro de la BDD ne fonctionne pas, tout est stocké en local sur les PC de pointage. La synchro peut être réalisée plus tard.

Lorsque les données remontent au serveur, des anomalies seront à corriger via l'interface administrateur : ajout, suppression.
Le classement devient définitif lorsque toutes les anomalies ont été rectifiées.

## Fonctionnalités

### Coté pointage
- Ajouter un dossard dans la base locale du point de passage
- Supprimer la dernière entrée (en cas d'erreur détectée par le pointeur)
- Visualiser les 20 derniers dossards passés

### Coté utilisateurs du site web :
- Affichage du classement Open 
- Affichage du classement Course 
- Recherche d'un dossard ou d'un nom de coureur et affichage de ses temps de passages
- Filtrage du classement par Année / Genre / Age

### Coté administrateur :
- Recherche d'un dossard, affichage et possibilité de supprimer ses temps
- Affichage de l'anomalie "Trop de passages >3" par checkpoint (vérification des erreurs de saisie)
- Affichage de l'anomalie "Passé à un pointage sans être passé par le précédent" (vérification des tricheurs)
- Ajout d'un temps à un participant pour le pointage sélectionné
- (Hors pointage) Uploader la liste des inscrits au format .csv pour l'Open et la course

## Bugs connus

## Authors

* **Nicolas Delvarre** - *Initial work* - [Mazef](https://framagit.org/Mazef)



