<!DOCTYPE html>
<html lang="fr">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<LINK rel="stylesheet" type="text/css" href="https://direct.100kmsteenwerck.fr/style.css">
<title>Administrer les inscriptions bénévoles</title>
</head>
<body>
<header>
<?php require_once('menu_admin_benevoles.php');?>
</header>
<h1><center>Liste des postes de bénévoles</center></h1>
<?php 
include('../../config.php');
// Connexion à la base de données du classement
$conn = mysqli_connect($servername, $username, $password, "benevoles");
// Vérifier la connexion
if ($conn->connect_error) {
    die("La connexion à la base de données du classement a échoué : " . $conn->connect_error);
}
// Fonctions
function generate_uuid_v4() {
    // Générer 16 octets (128 bits) aléatoires
    $data = random_bytes(16);

    // Version 4 de l'UUID
    $data[6] = chr(ord($data[6]) & 0x0f | 0x40);
    // Variante 1 de l'UUID
    $data[8] = chr(ord($data[8]) & 0x3f | 0x80);

    // Convertir les données binaires en chaîne hexadécimale
    return vsprintf('%s%s-%s-%s-%s-%s%s%s', str_split(bin2hex($data), 4));
}

// Modification des infos du poste bénévole en cas de POST de l'uuid
if (isset($_POST['uuid'])) {
	$uuid = $_POST['uuid'];
	$poste = $_POST['poste'];
	$horaire = $_POST['horaire'];
	$lieu = $_POST['lieu'];
	$maximum = $_POST['maximum'];
	$sql = "";
	if (isset($_POST['edition'])) {
		$sql = "UPDATE benevoles.postes SET Poste = '$poste', Lieu = '$lieu', Horaire = '$horaire', Maximum = '$maximum' WHERE uuid = '$uuid'";
	}
	if (isset($_POST['ajout'])) {
		$sql = "INSERT INTO benevoles.postes (`Poste`, `Lieu`, `Horaire`, `Maximum`, `UUID`) VALUES ('$poste','$lieu','$horaire','$maximum','$uuid')";
	}
	echo "La requete exécutée est : $sql";
	// Exécution de la requête SQL
	$result = $conn->query($sql);
	// Vérifier si la requête a réussi
	if (!$result) {
	    die("Erreur lors de l'exécution de la requête : " . $conn->error);
	}
	else {
		echo "Modifications prises en compte";
	}
}
// Suppression ou Affichage du formulaire en cas d'édition
if (isset($_GET['uuid'])) {
	$uuid = $_GET['uuid'];
	if (isset($_GET['suppression'])) {
		// Suppression du poste Recherche du poste en question avec son uid
		$sql = "DELETE from benevoles.postes WHERE UUID = '$uuid'";
		// Exécution de la requête SQL
		$result = $conn->query($sql);
		// Vérifier si la requête a réussi
		if (!$result) {
		    die("Erreur lors de l'exécution de la requête : " . $conn->error);
		}
		else {
			echo "Poste supprimé";
		}
		
	}
	if (isset($_GET['edition'])) {
		// Recherche du poste en question avec son uid
		$sql = "SELECT * from benevoles.postes WHERE UUID = '$uuid'";
		// Exécution de la requête SQL
		$result = $conn->query($sql);
		// Vérifier si la requête a réussi
		if (!$result) {
		    die("Erreur lors de l'exécution de la requête : " . $conn->error);
		}
		else {
			$row = $result->fetch_assoc();
			$poste = $row['Poste'];
			$horaire = $row['Horaire'];
			$lieu = $row['Lieu'];
			$maximum = $row['Maximum'];
			echo "
			<form action='liste_postes.php' id='benevoles' method='post'>
			    <label for='poste'>Poste :</label>
			    <input type='text' id='poste' name='poste' value='$poste' required>

			    <label for='horaire'>Horaire :</label>
			    <input type='text' id='horaire' name='horaire' value='$horaire' required>

			    <label for='lieu'>Lieu :</label>
			    <input type='text' id='lieu' name='lieu' value='$lieu' required>

			    <label for='maximum'>Places disponibles :</label>
			    <input type='number' id='maximum' name='maximum' value='$maximum' required>

			    <input type='hidden' id='uuid' name='uuid' value='$uuid'>
			    <input type='hidden' id='edition' name='edition' value='edition'>

			    <input type='submit' value='Valider les modifications'>";			
			}
	}
}


// Affichage du formulaire en cas d'ajout d'un nouveau poste
if (isset($_GET['ajout'])) {
	$uuid = generate_uuid_v4(); 
	if (isset($uuid)) {
		echo "
		<form action='liste_postes.php' id='benevoles' method='post'>
		    <label for='poste'>Poste :</label>
		    <input type='text' id='poste' name='poste' required>

		    <label for='horaire'>Horaire :</label>
		    <input type='text' id='horaire' name='horaire' required>

		    <label for='lieu'>Lieu :</label>
		    <input type='text' id='lieu' name='lieu' required>

		    <label for='maximum'>Places disponibles :</label>
		    <input type='number' id='maximum' name='maximum' required>

		    <input type='hidden' id='uuid' name='uuid' value='$uuid'>
		    <input type='hidden' id='ajout' name='ajout' value='ajout'>

		    <input type='submit' value='Valider les modifications'>";			
		}
}

// Affichage de la liste des postes de bénévoles
$sql = "SELECT * from benevoles.postes";
// Exécution de la requête SQL
$result = $conn->query($sql);
// Vérifier si la requête a réussi
if (!$result) {
    die("Erreur lors de l'exécution de la requête : " . $conn->error);
}
else {
echo "<table border=1 class=tableBenevoles><tr><th>Poste</th><th>Horaire</th><th>Lieu</th><th>Nombre maximum</th><th><a href='liste_postes.php?ajout=1'>Ajout</a></th></tr>";
while ($row = $result->fetch_assoc()) {
	echo "<tr><td>".$row['Poste']."</td><td>".$row['Horaire']."</td><td>".$row['Lieu']."</td><td>".$row['Maximum']."</td><td><a href='liste_postes.php?edition=1&uuid=".$row['UUID']."'>Editer</a> | <a href='liste_postes.php?suppression=1&uuid=".$row['UUID']."'>Supprimer</tr>";
	}
}
echo "</table>";

$conn->close();
?>
</table>
</form>
</body>
</html>

