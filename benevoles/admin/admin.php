<!DOCTYPE html>
<html lang="fr">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<LINK rel="stylesheet" type="text/css" href="https://direct.100kmsteenwerck.fr/style.css">
<title>Administrer les inscriptions bénévoles</title>
</head>
<body>
<header>
<?php require_once('menu_admin_benevoles.php');?>
</header>
<h1><center>Administrer les inscriptions bénévoles</center></h1>
<?php 
include('../../config.php');
// Connexion à la base de données du classement
$conn = mysqli_connect($servername, $username, $password, "benevoles");
// Vérifier la connexion
if ($conn->connect_error) {
    die("La connexion à la base de données du classement a échoué : " . $conn->connect_error);
}

// Affichage du formulaire en cas d'ajout d'un nouveau bénévole
if (isset($_GET['edition'])) {
	$nom = $_GET['nom'];
	$prenom = $_GET['prenom'];
	$dateNaissance = $_GET['dateNaissance'];
	// Recherche du benevole en question
	$sql = "SELECT * from `2024-benevoles` WHERE Nom = '$nom' AND Prenom = '$prenom' AND DateNaissance = '$dateNaissance'";
	// Exécution de la requête SQL
	$result = $conn->query($sql);
	// Vérifier si la requête a réussi
	if (!$result) {
	    die("Erreur lors de l'exécution de la requête : " . $conn->error);
	}
	else {
		$row = $result->fetch_assoc();
		$telephone = $row['Telephone'];
		$email = $row['Email'];
		echo "
		<form action='admin.php' id='benevoles' method='post'>
		    <label for='nom'>Nom :</label>
		    <input type='text' id='nom' name='nom' value='$nom' required>

		    <label for='prenom'>Prenom :</label>
		    <input type='text' id='prenom' name='prenom' value='$prenom' required>

		    <label for='lieu'>DateNaissance :</label>
		    <input type='date' id='dateNaissance' name='dateNaissance' value='$dateNaissance' required>

		    <label for='telephone'>Telephone :</label>
		    <input type='tel' id='telephone' name='telephone' value='$telephone' required>

		    <label for='email'>Email :</label>
		    <input type='email' id='email' name='email' value='$email' required>

                    <input type='hidden' id='edition' name='edition' value='1'>
                    <input type='hidden' id='oldNom' name='oldNom' value='$nom'>
                    <input type='hidden' id='oldPrenom' name='oldPrenom' value='$prenom'>
                    <input type='hidden' id='oldDateNaissance' name='oldDateNaissance' value='$dateNaissance'>

		    <input type='submit' value='Valider les modifications'>";
		}
}
if (isset($_POST['edition'])) {
	$oldNom = $_POST['oldNom'];
	$oldPrenom = $_POST['oldPrenom'];
	$oldDateNaissance = $_POST['oldDateNaissance'];
	$nom = $_POST['nom'];
	$prenom = $_POST['prenom'];
	$dateNaissance = $_POST['dateNaissance'];
	$telephone = $_POST['telephone'];
	$email = $_POST['email'];
	// Edition du benevole en question
	$sql = "UPDATE `2024-benevoles` SET Nom = '$nom', Prenom = '$prenom', DateNaissance = '$dateNaissance', Telephone = '$telephone', Email = '$email' WHERE Nom = '$oldNom' AND Prenom = '$oldPrenom' AND DateNaissance = '$oldDateNaissance'";
	// Exécution de la requête SQL
	$result = $conn->query($sql);
	// Vérifier si la requête a réussi
	if (!$result) {
	    die("Erreur lors de l'exécution de la requête : " . $conn->error);
	}
	else {
		echo "Le bénévole a été modifié<br>";
	}
}
// Suppression d'un bénévole ou d'une inscription
if (isset($_GET['nom'])) {
	// Suppression d'une inscription
	if (isset($_GET['uuid'])) {
		$uuid = $_GET['uuid'];
		$nom = $_GET['nom'];
		$prenom = $_GET['prenom'];
		$sql = "SELECT Inscriptions from `2024-benevoles` WHERE Nom = '$nom' AND Prenom = '$prenom'";
		// Exécution de la requête SQL
		$result = $conn->query($sql);
		// Vérifier si la requête a réussi
		if (!$result) {
		    die("Erreur lors de l'exécution de la requête : " . $conn->error);
		}
		else {
				$row = $result->fetch_assoc();
			//while ($row = $result->fetch_assoc()) {
				$oldInscriptions = $row['Inscriptions'];
				//echo $oldInscriptions;
				$newInscriptions = str_replace($uuid, "", $oldInscriptions);
				//echo $newInscriptions;
				$sql = "UPDATE `2024-benevoles` SET Inscriptions = '$newInscriptions' WHERE Nom = '$nom' AND Prenom = '$prenom'";
				// Exécution de la requête SQL
				$result = $conn->query($sql);
				// Vérifier si la requête a réussi
				if (!$result) {
				    die("Erreur lors de l'exécution de la requête : " . $conn->error);
				}
				else{
					echo "L'inscription de $nom $prenom sur l'uuid $uuid a été supprimée.";
				}

			}
		
	}
	// Suppression d'un bénévole
	else{
		if (isset($_GET['suppression'])){
			$nom = $_GET['nom'];
			$prenom = $_GET['prenom'];
			$sql = "DELETE from `2024-benevoles` WHERE Nom = '$nom' AND Prenom = '$prenom'";
			// Exécution de la requête SQL
			$result = $conn->query($sql);
			// Vérifier si la requête a réussi
			if (!$result) {
			    die("Erreur lors de l'exécution de la requête : " . $conn->error);
			}
			else {
				echo "$nom $prenom Supprimé";
			}
		}
	}
}

// Suppression d'une inscription
if (isset($_GET['poste'])) {
	$poste = $_GET['poste'];
	$prenom = $_GET['prenom'];
	$sql = "DELETE from `2024-benevoles` WHERE Nom = '$nom' AND Prenom = '$prenom'";
	// Exécution de la requête SQL
	$result = $conn->query($sql);
	// Vérifier si la requête a réussi
	if (!$result) {
	    die("Erreur lors de l'exécution de la requête : " . $conn->error);
	}
	else {
		echo "$nom $prenom Supprimé";
	}
}


// Affichage de la liste des bénévoles
$sql = "SELECT * from `2024-benevoles`";
// Exécution de la requête SQL
$result = $conn->query($sql);
// Vérifier si la requête a réussi
if (!$result) {
    die("Erreur lors de l'exécution de la requête : " . $conn->error);
}
else {
echo "<table border=1 class=tableBenevoles><tr><th>Nom</th><th>Prénom</th><th>Email</th><th>Telephone</th><th>DateNaissance</th><th>Postes</th></tr>";
while ($row = $result->fetch_assoc()) {
	echo "<tr><td>".$row['Nom']."</td><td>".$row['Prenom']."</td><td>".$row['Email']."</td><td>".$row['Telephone']."</td><td>".$row['DateNaissance']."</td><td><table>";
	$Inscriptions = explode(",", $row['Inscriptions']);
	foreach ($Inscriptions as $uuid){
		$sql2 = "SELECT * FROM benevoles.postes WHERE UUID = '$uuid'";
		// Exécution de la requête SQL
		$result2 = $conn->query($sql2);
		// Vérifier si la requête a réussi
		if (!$result2) {
		    die("Erreur lors de l'exécution de la requête : " . $conn->error);
		}
		else {
			while ($row2 = $result2->fetch_assoc()) {
				echo"<tr><td>".$row2['Poste']."</td><td>".$row2['Horaire']."</td><td>".$row2['Lieu']."</td><td><a href='admin.php?uuid=".$uuid."&nom=".$row['Nom']."&prenom=".$row['Prenom']."'>Supprimer inscription</td></tr>";
				}
			}
		}
			echo "</table><a href='admin.php?edition=1&nom=".$row['Nom']."&prenom=".$row['Prenom']."&dateNaissance=".$row['DateNaissance']."'>Editer le Bénévole</a> | ";
			echo "<a href='admin.php?suppression=1&nom=".$row['Nom']."&prenom=".$row['Prenom']."'>Supprimer le Bénévole</td>";
	}
}
echo "</table>";

$conn->close();
?>
</table>
</form>
</body>
</html>

