<!DOCTYPE html>
<html lang="fr">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<LINK rel="stylesheet" type="text/css" href="https://direct.100kmsteenwerck.fr/style.css">
<title>Administrer les inscriptions bénévoles</title>
</head>
<body>
<header>
<?php require_once('menu_admin_benevoles.php');?>
</header>
<h1><center>Administrer les inscriptions bénévoles</center></h1>
<?php
include('../../config.php');
// Connexion à la base de données du classement
$conn = mysqli_connect($servername, $username, $password, "benevoles");
// Vérifier la connexion
if ($conn->connect_error) {
    die("La connexion à la base de données du classement a échoué : " . $conn->connect_error);
}

// Récupération de la liste des bénévoles
$sql = "SELECT * from `2024-benevoles`";
// Exécution de la requête SQL
$result = $conn->query($sql);
// Vérifier si la requête a réussi
if (!$result) {
    die("Erreur lors de l'exécution de la requête : " . $conn->error);
}
else {
while ($row = $result->fetch_assoc()) {
	$message = "<html>
	<head>
	  <title>Email récapitulatif du bénévolat des 100kms de Steenwerck</title>
	    <style>
	    h1 {
	      color: blue;
	    }
	    p {
	      font-size: 16px;
	      color: #333;
	    }
	    p.background {
	      background-color: yellow;
	    }
	    a {
	      color: red;
	    }
		th {
			background-color: #fbbd13;
		}
        table {
            width: 100%;
            margin: 10px auto;
            border-collapse: collapse;
            box-shadow: 0 0 20px rgba(0, 0, 0, 0.1);
            background-color: #fff;
        }
	  </style>
	</head>
	<body>
	<p>Bonjour,
	un grand merci pour ta participation en temps que bénévole de cette édition des 100kms de Steenwerck.
	Voici un récapitulatif de tes inscriptions.</p>
	<table border=1 class=tableBenevoles><tr><th>Nom</th><th>Prénom</th><th>Email</th><th>Telephone</th><th>DateNaissance</th></tr>";
	$to = $row['Email'];
        $text = "<tr><td>".$row['Nom']."</td><td>".$row['Prenom']."</td><td>".$row['Email']."</td><td>".$row['Telephone']."</td><td>".$row['DateNaissance']."</td></table><table border=1><th>Poste</th><th>Horaire</th><th>Lieu</th>";
	$message .= $text;
        $Inscriptions = explode(",", $row['Inscriptions']);
        foreach ($Inscriptions as $uuid){
                $sql2 = "SELECT * FROM benevoles.postes WHERE UUID = '$uuid'";
                // Exécution de la requête SQL
                $result2 = $conn->query($sql2);
                // Vérifier si la requête a réussi
                if (!$result2) {
                    die("Erreur lors de l'exécution de la requête : " . $conn->error);
                }
                else {
                        while ($row2 = $result2->fetch_assoc()) {
                                $text = "<tr><td>".$row2['Poste']."</td><td>".$row2['Horaire']."</td><td>".$row2['Lieu']."</td></tr>";
				$message .= $text;
                                }
                        }
                }
                        $text = "</table></body></html>";
			$message .= $text;
			$subject = 'Rappel du bénévolat des 100kms de Steenwerck';
			$headers = 'From: expediteur@example.com' . "\r\n" .
				   'MIME-Version: 1.0' . "\r\n" .
				   'Content-type:text/html;charset=UTF-8' . "\r\n" .
				   'Reply-To: expediteur@example.com' . "\r\n" .
				   'X-Mailer: PHP/' . phpversion();

			// Envoi du mail
			if (mail($to, $subject, $message, $headers)) {
			    echo "Email envoyé à $to avec succès.<br>";
			} else {
			    echo 'Échec de l\'envoi de l\'email.<br>';
			}
		}
}

echo $message;

?>

