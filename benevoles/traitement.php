<!DOCTYPE html>
<html lang="fr">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<LINK rel="stylesheet" type="text/css" href="https://direct.100kmsteenwerck.fr/style.css">
<title>S'inscrire comme bénévole</title>
</head>
<body>
<header>
<?php require_once('menu_benevoles.php');?>
</header>

<?php
$nom = isset($_POST['nom']) ? htmlspecialchars($_POST['nom']) : '%';
$prenom = isset($_POST['prenom']) ? htmlspecialchars($_POST['prenom']) : '%';
$email = isset($_POST['email']) ? htmlspecialchars($_POST['email']) : '%';
$telephone = isset($_POST['telephone']) ? htmlspecialchars($_POST['telephone']) : '%';
$dateNaissance = isset($_POST['dateNaissance']) ? htmlspecialchars($_POST['dateNaissance']) : '%';
include('../config.php');
$uuidList="";
// Vérifier si des cases ont été sélectionnées
if(isset($_POST['cases']) && !empty($_POST['cases'])) {
    echo "<h2><center>Récapitulatif des nouvelles inscriptions :</h2></center>";
    // Parcourir les cases sélectionnées
    echo "<table><tr><th>Poste</th><th>Horaire</th><th>Lieu</th></tr>";
    foreach($_POST['cases'] as $uuid) {
	$uuidList=$uuidList.",".$uuid;
        //echo "<li>Case $uuid</li>";
        // Ici, vous pouvez ajouter le code pour enregistrer les inscriptions dans une base de données, par exemple
	// Connexion à la base de données du classement
	$conn = mysqli_connect($servername, $username, $password, "benevoles");
	// Vérifier la connexion
	if ($conn->connect_error) {
	    die("La connexion à la base de données du classement a échoué : " . $conn->connect_error);
	}
	$sql = "SELECT * from benevoles.postes WHERE UUID = '$uuid'";
        // Exécution de la requête SQL
        $result = $conn->query($sql);
	// Vérifier si la requête a réussi
	if (!$result) {
	    die("Erreur lors de l'exécution de la requête : " . $conn->error);
	}
	else{
	   while ($row = $result->fetch_assoc()) {
		$poste = $row['Poste'];
		$lieu = $row['Lieu'];
		$horaire = $row['Horaire'];
			//echo "<li>Vous êtes inscrits en temps que $poste de $horaire à l'emplacement $lieu. Total d'inscription sur ce créneau : $disponible</li>";
			echo "<tr><td>$poste</td><td>$horaire</td><td>$lieu</td></tr>";
		}
	}
    }
    echo "</table>";
    // Inscription en BDD
    // Vérification que le benevole n'existe pas déja
    $sql = "SELECT Inscriptions FROM `2024-benevoles` WHERE Nom = '$nom' AND Prenom = '$prenom' AND DateNaissance = '$dateNaissance'";
    $result = $conn->query($sql);
    if (!$result) {
    	die("Erreur lors de l'exécution de la requête : " . $conn->error);
    }
    else {
    	$row = $result->fetch_assoc();
	if ($result->num_rows > 0) {
		$Inscriptions = $row['Inscriptions'];
	}
	else {
		$Inscriptions = NULL;
	}
	if ($Inscriptions){
		// Le bénévole existe déjà, on lui modifie son UUID
		#echo "L'utilisateur existe déjà et s'est inscrit à : <br>$Inscriptions";
		$newInscriptions=$Inscriptions.",".$uuidList;
		#echo "<br>$newInscriptions";
		//$sql = "UPDATE `2024-benevoles` SET Inscriptions = '$newInscriptions' WHERE Nom = '$nom' AND Prenom = '$prenom' AND DateNaissance = '$dateNaissance'";
		$sql = "UPDATE `2024-benevoles` SET Inscriptions = ? WHERE Nom = ? AND Prenom = ? AND DateNaissance = ?";
	        //$result = $conn->query($sql);
		$stmt = $conn->prepare($sql);
		// Assurez-vous que la requête préparée a réussi
		if (!$stmt) {
		    die("Erreur lors de la préparation de la requête : " . $conn->error);
		}

		// Remplacez votre ancienne liaison de paramètre par celui-ci
		$stmt->bind_param("ssss", $newInscriptions, $nom, $prenom, $dateNaissance);
		if ($stmt->execute()){
			echo "<br>Les inscriptions de $nom $prenom ont été modifiées"; 
		}
		else{
			die("Erreur lors de l'exécution de la requête : " . $conn->error);
		}

	}
	else {
		// Inscription en BDD d'un nouveau bénévole
	        //$sql = "INSERT INTO `2024-benevoles` (`Nom`, `Prenom`, `Email`, `Telephone`, `DateNaissance`, `Inscriptions`) VALUES ('$nom','$prenom','$email','$telephone','$dateNaissance','$uuidList')";
	        $sql = "INSERT INTO `2024-benevoles` (`Nom`, `Prenom`, `Email`, `Telephone`, `DateNaissance`, `Inscriptions`) VALUES (?,?,?,?,?,?)";

	        //$result = $conn->query($sql);
		$stmt = $conn->prepare($sql);
		// Assurez-vous que la requête préparée a réussi
		if (!$stmt) {
		    die("Erreur lors de la préparation de la requête : " . $conn->error);
		}

		// Remplacez votre ancienne liaison de paramètre par celui-ci
		$stmt->bind_param("ssssss", $nom, $prenom, $email, $telephone, $dateNaissance, $uuidList);
		// Exécutez la requête
		if ($stmt->execute()){
			echo "Un nouveau bénévole a été inscrit : $nom $prenom";
	        }
		else {
			die("Erreur lors de l'exécution de la requête : " . $conn->error);
		}
	}
    }
}
else {
    echo "<h2>Aucune case sélectionnée !</h2>";
}
?>

<center><a href="benevoles.php">Retour au formulaire</a></center>

</body>
</html>

