<!DOCTYPE html>
<html lang="fr">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<LINK rel="stylesheet" type="text/css" href="https://direct.100kmsteenwerck.fr/style.css">
<title>S'inscrire comme bénévole</title>
</head>
<body>
<header>
<?php require_once('menu_benevoles.php');?>
</header>
<h1><center>S'inscrire comme bénévole</center></h1>
<form action='traitement.php' id='benevoles' method='post'>
    <label for="nom">Nom :</label>
    <input type="text" id="nom" name="nom" required>

    <label for="prenom">Prénom :</label>
    <input type="text" id="prenom" name="prenom" required>

    <label for="email">Email :</label>
    <input type="email" id="email" name="email" required>

    <label for="telephone">Téléphone :</label>
    <input type="tel" id="telephone" name="telephone" pattern="[0-9]{10}" placeholder="Format : 0623456789">

    <label for="dateNaissance">Date de naissance :</label>
    <input type="date" id="dateNaissance" name="dateNaissance" required>

<?php 
include('../config.php');
// Connexion à la base de données du classement
$conn = mysqli_connect($servername, $username, $password, "benevoles");
// Vérifier la connexion
if ($conn->connect_error) {
    die("La connexion à la base de données du classement a échoué : " . $conn->connect_error);
}


function AfficherTableau($conn,$Lieux,$Horaires,$Poste){
echo "<table border=1 class=tableBenevoles><tr><th>Lieux/Horaires</th>";
foreach ($Lieux as $lieu){
echo "<th>".$lieu."</th>";
}
foreach ($Horaires as $horaire){
echo "<tr><td>".$horaire."</td>";
foreach ($Lieux as $lieu){
	$sql = "SELECT Maximum,UUID from benevoles.postes WHERE Poste = '$Poste' AND Horaire = '$horaire' AND Lieu = '$lieu'";
        	// Exécution de la requête SQL
		$result = $conn->query($sql);
		// Vérifier si la requête a réussi
		if (!$result) {
		    die("Erreur lors de l'exécution de la requête : " . $conn->error);
		}
		else {
		// Ajout d'une case avec les données récupérées
			while ($row = $result->fetch_assoc()) {
			$uuid=$row['UUID'];
			// Recherche du nombre d'inscrits pour ce poste dans la liste des bénévoles
			$sql = "SELECT COUNT(*) as total FROM `2024-benevoles` WHERE Inscriptions LIKE '%$uuid%'";
			$result = $conn->query($sql);
			if (!$result) {
			    die("Erreur lors de l'exécution de la requête : " . $conn->error);
			}
			else {
			$disponibilite = $result->fetch_assoc();
			$disponible = $disponibilite['total'];
			if ($disponible >= $row['Maximum']){
				echo "<td class='green'>".$disponible." / ".$row['Maximum'];
			}
				else{
					echo "<td class='red'>";
					echo "<input type='checkbox' name='cases[]' value=$uuid>";
					echo $disponible." / ".$row['Maximum'];
				}
				//echo "$uuid";
				// Afficher les noms
				if ($disponible > 0){
					$sql2 = "SELECT Nom,Prenom from `2024-benevoles` WHERE Inscriptions LIKE '%${uuid}%'";
					// Exécution de la requête SQL
					$result2 = $conn->query($sql2);
					// Vérifier si la requête a réussi
					if (!$result2) {
					    die("Erreur lors de l'exécution de la requête : " . $conn->error);
					}
					else {
						if ($result2->num_rows > 0) {
						while ($inscrits = $result2->fetch_assoc()) {
							echo "<br>".$inscrits['Nom']." ".$inscrits['Prenom'];
						}}
					}
				}

			}
			// Fermer la case
			echo "</td>";
		}
		}
	}
	echo "</tr>";
}
echo "</table><table border='2'>";
}

$Postes = array("Signaleur","Signaleurs aux départs","Soins", "Divers - Avant", "Divers - Pendant", "Divers - Après","Salle des sports divers","Course des jeunes", "Pointage", "Ravitaillement","Responsables ravitaillement");
foreach ($Postes as $poste){
	$Lieux=[];
	$Horaires=[];
	echo "<h1>$poste</h1>";
				$sql = "SELECT DISTINCT Lieu FROM `postes` WHERE Poste = '$poste'";
				// Exécution de la requête SQL
				$result = $conn->query($sql);
				// Vérifier si la requête a réussi
				if (!$result) {
				    die("Erreur lors de l'exécution de la requête : " . $conn->error);
				}
				else {
					    while($row = $result->fetch_assoc()) {
    						    $Lieux[] = $row['Lieu'];
						  //  echo "Lieu : ".$row['Lieu']."<br>";
    						}
				}
				$sql2 = "SELECT DISTINCT Horaire FROM `postes` WHERE Poste = '$poste'";
				// Exécution de la requête SQL
				$result2 = $conn->query($sql2);
				// Vérifier si la requête a réussi
				if (!$result2) {
				    die("Erreur lors de l'exécution de la requête : " . $conn->error);
				}
				else {
					    while($row2 = $result2->fetch_assoc()) {
    						    $Horaires[] = $row2['Horaire'];
						    //echo "toto ". $row2['Horaire'];
    						}
				}
AfficherTableau($conn,$Lieux,$Horaires,$poste);
}



echo "<input type='submit' value='Valider mon inscription'>";

$conn->close();
?>
</table>
</form>
</body>
</html>

