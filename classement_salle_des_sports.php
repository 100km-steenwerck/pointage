<?php
// Paramètres de connexion à la base de données
require_once 'config.php';
$course = isset($_GET['course']) ? htmlspecialchars($_GET['course']) : '%';
$sexe = isset($_GET['sexe']) ? htmlspecialchars($_GET['sexe']) : '%';
$anneeStart = isset($_GET['anneeStart']) ? htmlspecialchars($_GET['anneeStart']) : '1800';
$anneeStop = isset($_GET['anneeStop']) ? htmlspecialchars($_GET['anneeStop']) : '2100';
$annee = isset($_GET['annee']) ? htmlspecialchars($_GET['annee']) : "{$table}";
$age = isset($_GET['age']) ? htmlspecialchars($_GET['age']) : "1";
$steenwerck = isset($_GET['steenwerck']) ? htmlspecialchars($_GET['steenwerck']) : "all";
?>
<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
<LINK rel="stylesheet" type="text/css" href="style.css">
    <title>Resultats</title>
</head>
<body>
<header>
<div class="container">
<?php require_once('menu.php');?>
</header>
<?php
// Définit l'heure du départ selon la course
if ($course == "Course"){
	$dateDepart = $dateCourse[$annee];
}
else{
	$dateDepart = $dateOpen[$annee];
}
if ($sexe == "all" || $sexe == ""){
	$sexe = "%";
}
if ($annee == "all" || $annee == ""){
	$annee = $table;
}
# Création de la requête SteenwerckFilter pour filtreer sur les Steenwerckois dans la requete SQL
$steenwerckFilter = "1";
$notSteenwerckFilter = "0";
if ($steenwerck == "1"){
	$notSteenwerckFilter = "1";
}
if ($steenwerck == "0"){
	$steenwerckFilter = "0";
}
switch ($age) {
    case "Juniors":
	$anneeStart = $annee - 19;
	$anneeStop = $annee - 17; # J'enleve 1 pour les 18 ans en début d'année qui participeraient
        break;
    case "Espoirs":
	$anneeStart = $annee - 22;
	$anneeStop = $annee - 20;
        break;
    case "Seniors":
	$anneeStart = $annee - 34;
	$anneeStop = $annee - 23;
        break;
    case "M0":
	$anneeStart = $annee - 39;
	$anneeStop = $annee - 35;
        break;
    case "M1":
	$anneeStart = $annee - 44;
	$anneeStop = $annee - 40;
        break;
    case "M2":
	$anneeStart = $annee - 49;
	$anneeStop = $annee - 45;
        break;
    case "M3":
	$anneeStart = $annee - 54;
	$anneeStop = $annee - 50;
        break;
    case "M4":
	$anneeStart = $annee - 59;
	$anneeStop = $annee - 55;
        break;
    case "M5":
	$anneeStart = $annee - 64;
	$anneeStop = $annee - 60;
        break;
    case "M6":
	$anneeStart = $annee - 69;
	$anneeStop = $annee - 65;
        break;
    case "M7":
	$anneeStart = $annee - 74;
	$anneeStop = $annee - 70;
        break;
    case "M8":
	$anneeStart = $annee - 79;
	$anneeStop = $annee - 75;
        break;
    case "M9":
	$anneeStart = $annee - 84;
	$anneeStop = $annee - 80;
        break;
    case "M10":
	$anneeStart = $annee - 200;
	$anneeStop = $annee - 85;
        break;

    default:
        break;
}
// Connexion à la base de données du classement
$conn = mysqli_connect($servername, $username, $password, $db);

// Vérifier la connexion
if ($conn->connect_error) {
    die("La connexion à la base de données du classement a échoué : " . $conn->connect_error);
}

    echo "<div class='filtres'>
	<form id='filtres' action='?course=". $course . "' method='get'>
	<input type='hidden' id='course' name='course' value='$course'>
	<input type='hidden' id='sexe' name='sexe' value='$sexe'>
	<input type='hidden' id='age' name='age' value='$age'>
	<input type='hidden' id='steenwerck' name='steenwerck' value='$steenwerck'>
	<select id='filtres' name='annee'>
	<option value=''>Année</option>
	<option value='$table'";if ($annee == "$table") { echo "selected";}; echo ">$table</option>";
	for ($i = $table -1 ; $i >= 2024; $i--) {
		echo "<option value='$i'";if ($annee == "$i") { echo "selected";}; echo ">$i</option>";
	}
	echo "</select>
        <button type='submit' class='submit'>Filtrer</button> 
    </form>
    <form id='filtres' action='?course=". $course . "' method='get'>
	<input type='hidden' id='course' name='course' value='$course'>
	<input type='hidden' id='annee' name='annee' value='$annee'>
	<input type='hidden' id='age' name='age' value='$age'>
	<input type='hidden' id='steenwerck' name='steenwerck' value='$steenwerck'>
	<select id='filtres' name='sexe'>
            <option value=''>Genre</option>
	    <option value='H'";if ($sexe == "H") { echo "selected";}; echo ">Hommes</option>
	    <option value='F'";if ($sexe == "F") { echo "selected";}; echo ">Femmes</option>
		    </select>
        <button type='submit' class='submit'>Filtrer</button> 
    </form>
    <form id='filtres' action='?course=". $course . "' method='get'>
	<input type='hidden' id='course' name='course' value='$course'>
	<input type='hidden' id='annee' name='annee' value='$annee'>
	<input type='hidden' id='sexe' name='sexe' value='$sexe'>
	<input type='hidden' id='age' name='age' value='$age'>
	<select id='filtres' name='steenwerck'>
            <option value=''>Adresse</option>
	    <option value='1'";if ($steenwerck == "1") { echo "selected";}; echo ">Steenwerck</option>
	    <option value='0'";if ($steenwerck == "0") { echo "selected";}; echo ">Autres</option>
		    </select>
        <button type='submit' class='submit'>Filtrer</button> 
    </form>
    <form id='filtres' action='classement_salle_des_sports.php' method='get'>
	<input type='hidden' id='course' name='course' value='$course'>
	<input type='hidden' id='sexe' name='sexe' value='$sexe'>
	<input type='hidden' id='annee' name='annee' value='$annee'>
	<input type='hidden' id='steenwerck' name='steenwerck' value='$steenwerck'>
	<select id='filtres' name='age'>
            <option value=''>Catégorie d'âge</option>
	    <option value='Juniors'";if ($age == "Juniors") { echo "selected";}; echo ">Juniors</option>
	    <option value='Espoirs'";if ($age == "Espoirs") { echo "selected";}; echo ">Espoirs</option>
	    <option value='Seniors'";if ($age == "Seniors") { echo "selected";}; echo ">Seniors</option>
	    <option value='M0'";if ($age == "M0") { echo "selected";}; echo ">Masters M0</option>
	    <option value='M1'";if ($age == "M1") { echo "selected";}; echo ">Masters M1</option>
	    <option value='M2'";if ($age == "M2") { echo "selected";}; echo ">Masters M2</option>
	    <option value='M3'";if ($age == "M3") { echo "selected";}; echo ">Masters M3</option>
	    <option value='M4'";if ($age == "M4") { echo "selected";}; echo ">Masters M4</option>
	    <option value='M5'";if ($age == "M5") { echo "selected";}; echo ">Masters M5</option>
	    <option value='M6'";if ($age == "M6") { echo "selected";}; echo ">Masters M6</option>
	    <option value='M7'";if ($age == "M7") { echo "selected";}; echo ">Masters M7</option>
	    <option value='M8'";if ($age == "M8") { echo "selected";}; echo ">Masters M8</option>
	    <option value='M9'";if ($age == "M9") { echo "selected";}; echo ">Masters M9</option>
	    <option value='M10'";if ($age == "M10") { echo "selected";}; echo ">Masters M10</option>
		    </select>
        <button type='submit' class='submit'>Filtrer</button> 
    </form>
    </div>
    <div class='filtres'>
	<form id='filtres' action='classement_salle_des_sports.php' method='get'>
	<input type='hidden' id='course' name='course' value='$course'>
        <button type='submit' class='submit'>Réinitialiser tous les filtres</button> 
    <form>
    </div>";

echo "<h2><center>Classement provisoire</center></h2>";





	// Exécution de la requête SQL
$sql = "
SELECT
	ToutesLesBases.Dossard,
RANK() OVER (
ORDER BY Nombre_de_Presences DESC, temps_total ASC
    ) Position,
    c.Nom,
    c.Prenom,
    c.Sexe,
    c.Annee,
    c.Course,
    c.Steenwerck,
	MAX(Date) AS date_arrivee,
    TIMEDIFF(MAX(Date), '$dateDepart') AS temps_total,
    ROUND (( 100 / 15 * COUNT(*) / TIME_TO_SEC(TIMEDIFF(MAX(Date), '$dateDepart')) * 3600),2) AS vitesse_moyenne,
    COUNT(*) as Nombre_de_Presences,
    ROUND (100 / 15 * COUNT(*),0) AS Distance
FROM (
    SELECT Dossard,Date FROM Pointage5.$annee
) AS ToutesLesBases
JOIN
    coureurs.$annee c ON ToutesLesBases.Dossard = c.Dossard
WHERE c.Course LIKE ? AND c.Sexe LIKE ? AND c.Annee > ? AND c.Annee < ? AND ( c.Steenwerck = ? OR c.Steenwerck = ? ) 
GROUP BY Dossard
ORDER BY Nombre_de_Presences DESC, temps_total ASC;";

//$result = $conn->query($sql);

$stmt = $conn->prepare($sql);

// Assurez-vous que la requête préparée a réussi
if (!$stmt) {
    die("Erreur lors de la préparation de la requête : " . $conn->error);
}

// Remplacez votre ancienne liaison de paramètre par celui-ci
$stmt->bind_param("ssiiss", $course, $sexe, $anneeStart, $anneeStop, $steenwerckFilter, $notSteenwerckFilter);
#$stmt->bind_param("ssiis", $course, $sexe, $anneeStart, $anneeStop, $steenwerckFilter);

// Exécutez la requête
$stmt->execute();

// Obtenez le résultat
$result = $stmt->get_result();

// Vérifier si la requête a réussi
if (!$result) {
    die("Erreur lors de l'exécution de la requête : " . $conn->error);
}

// Afficher le classement
echo "<table id='classement' border='1'>
    <tr>
        <th>Position</th>
        <th>Dossard</th>
        <th>Nom</th>
        <th>Prénom</th>
	<th>Sexe</th>
	<th>Année Naissance</th>
	<th>Steenwerckois</th>
	<th>Distance approx.</th>
        <th>Temps</th>
        <th>Vitesse</th>
    </tr>";

while ($row = $result->fetch_assoc()) {
    echo "<tr onclick=window.location='recherche.php?dossard=" . $row["Dossard"] . "'>
            <td>" . $row["Position"] . "</td>
            <td>" . $row["Dossard"] . "</td>
            <td>" . $row["Nom"] . "</td>
            <td>" . $row["Prenom"] . "</td>
            <td>" . $row["Sexe"] . "</td>
            <td>" . $row["Annee"] . "</td>
            <td>" . $row["Steenwerck"] . "</td>
            <td>" . $row["Distance"] . " km</td>
            <td>" . $row["temps_total"] . "</td>
            <td>" . $row["vitesse_moyenne"] . " km/h</td>
          </tr>";
}

echo "</table>";

// Fermez la requête préparée
$stmt->close();
// Fermer la connexion à la base de données
$conn->close();

?>
</div>
<script>
    // Fonction pour récupérer la valeur d'un paramètre GET dans l'URL
    function getParametreGet(nomParametre) {
        var urlParams = new URLSearchParams(window.location.search);
        return urlParams.get(nomParametre);
    }

    // Fonction pour changer la couleur du bouton en fonction du paramètre GET 'course'
    function changerCouleurEnFonctionDeCourse() {
        var boutonOpen = document.getElementById("boutonOpen");
        var boutonCourse = document.getElementById("boutonCourse");
        var valeurCourse = getParametreGet('course');

        // Changer la couleur si la valeur de 'course' est égale à "Open"
        if (valeurCourse === "Open") {
            boutonOpen.style.backgroundColor = "#fbbd13"; // Changer la couleur en vert
        }
        if (valeurCourse === "Course") {
            boutonCourse.style.backgroundColor = "#fbbd13"; // Changer la couleur en vert
        }
    }

    // Appeler la fonction au chargement de la page
    changerCouleurEnFonctionDeCourse();


        // Fonction pour filtrer le tableau en fonction de la sélection
        function filterTable() {
            var filter = document.getElementById('sexe').value;
            var table = document.getElementById('classement');
            var rows = table.getElementsByTagName('tr');

            for (var i = 1; i < rows.length; i++) {
                var cells = rows[i].getElementsByTagName('td');
                var showRow = false;

                if (filter === 'all' || cells[4].innerText === filter) {
                    showRow = true;
                }

                rows[i].style.display = showRow ? '' : 'none';
            }
        }

</script>
</body>
</html>

