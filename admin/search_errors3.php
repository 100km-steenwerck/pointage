<?php require_once '../config.php';?>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <LINK rel="stylesheet" type="text/css" href="../style.css">
    <title>Formulaire</title>
</head>
<body>
<header>
<?php require_once('menu_admin.php');?>
</header>
<div class="container">
<table><?php

    echo "<tr><th>Dossard</th><th>Anomalie</th>";
    
// Créer la connexion à la base de données
$conn = mysqli_connect($servername, $username, $password, $db);

// Vérifier la connexion
if ($conn->connect_error) {
    die("La connexion a échoué : " . $conn->connect_error);
    echo "La connexion a échoué";
}

for ($dossard = 1; $dossard <= $nbrDossard; $dossard++) {
	$tour = 1;
	    // Anomalie vitesse
	    $sql = "
	SELECT
	*
	FROM (
	    SELECT '1' AS Pointage, Pointage1.$table.* FROM Pointage1.$table
	    UNION ALL
	    SELECT '2' AS Pointage, Pointage2.$table.* FROM Pointage2.$table
	    UNION ALL
	    SELECT '3' AS Pointage, Pointage3.$table.* FROM Pointage3.$table
	    UNION ALL
	    SELECT '4' AS Pointage, Pointage4.$table.* FROM Pointage4.$table
	    UNION ALL
	    SELECT '5' AS Pointage, Pointage5.$table.* FROM Pointage5.$table
	) AS ToutesLesBases
	WHERE ToutesLesBases.Dossard = $dossard
	GROUP BY Dossard,Date
	    ";
	    $result = $conn->query($sql);

	    if (empty($result)) {
		echo "Aucune anomalie détectée à ce pointage.";
	    } else {
		$datePrecedente = null;
		while($row = $result->fetch_assoc())
		{
			$dateCourante = new DateTime($row['Date']);
			#echo "$dateCourante - $datePrecedente<br>";
			if ($datePrecedente !== null ) {
				$diff = $datePrecedente->diff($dateCourante);
				//echo "$diff";
				$diffEnHeures = $diff->days * 24 + $diff->h + $diff->i / 60 + $diff->s / 3600;
				switch ($row["Pointage"]) {
				    case "1":
					    $distance = 9.316;
					    $tour++;
					break;

				    case "2":
					    $distance = 6.152;
					break;

				    case "3":
					    $distance = 6.810;
					break;
				    case "4":
					    $distance = 4.131;
					break;
				    case "5":
					    $distance = 5.731;
					break;
				    default:
					    $distance = (100 / 15);
					break;
				}
				# $vitesse = (100 / 15) / ($diffEnHeures); // Distance générique 100km divisé par 15 pointage
				$vitesse = $distance / ($diffEnHeures);
				// DEBUG
				// echo $vitesse."= ".$distance." / ".$diffEnHeures."<br>";
				if ($vitesse > 16){
					echo "<tr onclick=window.location='view_dossard_admin.php?dossard=" . $row["Dossard"] . "'><td>".$row["Dossard"]."</td><td>Vitesse trop rapide au pointage".$row["Pointage"]. " (". $lieu[$row["Pointage"]-1] .") du tour ". $tour . " : ".$vitesse."km/h</td></tr>";
				}
			}


			$datePrecedente = $dateCourante;
		}
		}
}
// Fermer la connexion
$conn->close();

?>
</table>
</div></body>
</html>
