<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
<LINK rel="stylesheet" type="text/css" href="../style.css">
    <title>Resultats</title>
</head>
<body>
<header>
<?php require_once('menu_admin.php');?>
</header>
<div class="container">
<?php
// Paramètres de connexion à la base de données
require_once '../config.php';
?>
<?php
### Recherche par nom de coureurs
$nom = isset($_POST['nom']) ? htmlspecialchars($_POST['nom']) : '';
if (isset($_POST['nom'])) {
    $pattern = "%".$nom."%";
    $conn = mysqli_connect($servername, $username, $password, $db);

    // Vérifier la connexion
    if ($conn->connect_error) {
        die("La connexion à la base de données du classement a échoué : " . $conn->connect_error);
    }
    // Utiliser des requêtes préparées pour éviter les attaques d'injection SQL
    $stmt = $conn->prepare("SELECT * FROM coureurs.$table WHERE Nom LIKE ? OR Prenom LIKE ?");
    $stmt->bind_param("ss", $pattern, $pattern);

    // Exécuter la requête préparée
    $stmt->execute();
    // Obtenez le résultat
    $result = $stmt->get_result();

    // Vérifier si la requête a réussi
    if (!$result) {
    	die("Erreur lors de l'exécution de la requête : " . $conn->error);
    }
    else {
	    // Afficher la liste des coureurs trouvés
	echo "<table border='1'>
	    <tr>
		<th>Dossard</th>
		<th>Nom</th>
		<th>Prénom</th>
		<th>Sexe</th>
		<th>Course</th>
	    </tr>";

	while ($row = $result->fetch_assoc()) {
	    echo "<tr onclick=window.location='view_dossard_admin.php?dossard=" . $row["Dossard"] . "'>
		    <td>" . $row["Dossard"] . "</td>
		    <td>" . $row["Nom"] . "</td>
		    <td>" . $row["Prenom"] . "</td>
		    <td>" . $row["Sexe"] . "</td>
		    <td>" . $row["Course"] . "</td>
		  </tr>";
	}

	echo "</table>";
    }

    // Fermer la requête et la connexion à la base de données
    $stmt->close();
    $conn->close();

} else {
### Si le dossard est entré, récupération du nom du coureur puis de ses temps
if (isset($_GET['dossard'])) {
$dossard = sanitizeNumber($_GET['dossard']);

// Récupération du nom du participant
    $conn = mysqli_connect($servername, $username, $password, $db);
    $sql = "SELECT Nom,Prenom,Course FROM coureurs.$table WHERE Dossard = $dossard";
    $result = $conn->query($sql);
    $row = $result->fetch_assoc();
    $courseParticipant = $row['Course'];

echo "<h1>Dossard $dossard - {$row['Nom']} {$row['Prenom']} - {$row['Course']} $table</h1>";

// Fonction de récupération des temps à chaque pointage
function getPassingTimes($conn, $table, $dossard)
{
    $sql = "SELECT * FROM `$table` WHERE Dossard = $dossard ORDER BY Date";
    $result = $conn->query($sql);

    $passingTimes = array();
    $tour = 1; // Initialiser le numéro du tour

    if ($result->num_rows > 0) {
     	    while ($row = $result->fetch_assoc()) {
            $passingTimes[] = array(
                'tour' => $tour,
                'Date' => $row['Date']
            );
            $tour++; // Incrémenter le numéro du tour
        }
    }

    return $passingTimes;
}
#echo "<table><tr><th>Lieu</th><th>Tour 1</th><th>Tour 2</th><th>Tour 3</th></tr>";
$p = 0; // Initialiser le numéro du tour
foreach ($dbname as $db) {
// Créer la connexion à la base de données
$conn = mysqli_connect($servername, $username, $password, $db);

// Vérifier la connexion
if ($conn->connect_error) {
    die("La connexion a échoué : " . $conn->connect_error);
}
$passingTimes = getPassingTimes($conn, $table, $dossard);
$passingTimess[$p+1] = getPassingTimes($conn, $table, $dossard);

### Premier affichage simple (supprimé)
#echo "<tr><td>$db - $lieu[$p]</td>";

#    if (empty($passingTimes)) {
#        echo "<td>Aucun temps</td>";
#    } else {
#        foreach ($passingTimes as $passingTime) {
#            echo "<td>{$passingTime['Date']}</td>";
#
#        }
#        echo "</tr>";
#    }
// Fermer la connexion
$conn->close();
$p++;
}
#echo "</table>";
}
}

if (isset($_GET['dossard'])) {
# Récupération des vitesses sans affichage, dans arrays tempsGlobal[] et vitesseGlobal[]
$t=1;
$v=1;
// On indique l'heure de départ selon la course participée
if ( $courseParticipant == "Course" ){
	$datePrecedente = new DateTime($dateCourse['2024']) ;
}
else{
	$datePrecedente = new DateTime($dateOpen['2024']) ;
}	
#echo "<table><tr><th></th><th>Pointage1</th><th>Pointage2</th><th>Pointage3</th><th>Pointage4</th><th>Pointage5</th>";
for ($j = 1; $j <= 4; $j++) {
#	echo "<tr><th>Tour".$j;
for ($i = 1; $i <= count($dbname); $i++) {
	if ($passingTimess[$i][$j-1]['Date']){
		$dateCourante = new DateTime($passingTimess[$i][$j-1]['Date']);
                        if ($datePrecedente != null AND $dateCourante != null ) {
                                $diff = $datePrecedente->diff($dateCourante);
                                //echo "$diff";
                                $diffEnHeures = $diff->days * 24 + $diff->h + $diff->i / 60 + $diff->s / 3600;
                                switch ($i) { // Numéro du pointage
                                    case "1":
					    if ( $i == 1 && $j == 1){
						    $distance = 12.895; // Au premier tour, on ajoute le tour dans le bourg
					    }
					    else {
                                            	    $distance = 9.316;
					    }
                                        break;
                                    case "2":
                                            $distance = 6.152;
                                        break;

                                    case "3":
                                            $distance = 6.810;
                                        break;
                                    case "4":
                                            $distance = 4.131;
                                        break;
                                    case "5":
					    $distance = 5.731;
                                        break;
                                    default:
                                            $distance = (100 / 15);
                                        break;
                                }
                                # $vitesse = (100 / 15) / ($diffEnHeures); // Distance générique 100km divisé par 15 pointage
                                $vitesse = round($distance / ($diffEnHeures+0.0000001),2);
	                	$datePrecedente = $dateCourante;
			}
			else{
				$datePrecedente = null;
				$vitesse = "-";
			}
	                $datePrecedente = $dateCourante;

#echo "<td>".$passingTimess[$i][$j-1]['Date']." (".$vitesse." km/h)</td>";
$tempsGlobal[$t]=$passingTimess[$i][$j-1]['Date'];
$t++;
$vitesseGlobal[$v]=$vitesse;
$v++;
}
		else
		{
#		echo "<td>Aucun temps</td>";
		$datePrecedente = null;
		$vitesse = "-";
		$tempsGlobal[$t]="-";
		$t++;
		$vitesseGlobal[$v]="-";
		$v++;
		}
}
#	echo "</tr>";
}
#echo "</table>";

### Affichage de tous les temps et vitesses
echo "<table><tr><th>Lieu</th><th colspan='2'>Tour 1</th><th colspan='2'>Tour 2</th><th colspan='2'>Tour 3</th></tr>";
for ($i = 1; $i <= 5; $i++) {
	echo "<tr><th>".$lieu[$i-1]."</td>";
	echo "<td>".$tempsGlobal[$i]." <a href='delete_time.php?dossard={$dossard}&time={$tempsGlobal[$i]}&pointage=Pointage$i'>DELETE</a></td><td>".$vitesseGlobal[$i]." km/h</td>";
	echo "<td>".$tempsGlobal[$i+5]." <a href='delete_time.php?dossard={$dossard}&time={$tempsGlobal[$i+5]}&pointage=Pointage$i'>DELETE</a></td><td>".$vitesseGlobal[$i+5]." km/h</td>";
	echo "<td>".$tempsGlobal[$i+10]." <a href='delete_time.php?dossard={$dossard}&time={$tempsGlobal[$i+10]}&pointage=Pointage$i'>DELETE</a></td><td>".$vitesseGlobal[$i+10]." km/h</td>";
	echo "<td>".$tempsGlobal[$i+15]." <a href='delete_time.php?dossard={$dossard}&time={$tempsGlobal[$i+15]}&pointage=Pointage$i'>DELETE</a></td><td>".$vitesseGlobal[$i+15]." km/h</td>";
	echo "</tr>";
}
echo "</table>";
}

### Fonctions de sécurité
function sanitizeNumber($data) {
    // Ensure the input is a numeric value
    if (is_numeric($data)) {
        // Convert the input to a float or integer, depending on the nature of the data
        return is_float($data + 0) ? floatval($data) : intval($data);
    } else {
        // Handle invalid input (you might log an error, set a default value, etc.)
        return 0; // Or any other default value or action you prefer
    }
}
?>
    <form action="view_dossard_admin.php" method="GET">
        <label for="number">Entrer le numéro de dossard:</label>
        <input type="number" id="number" name="dossard" min="0" max="9999" required>
                <br>
        <button class="submit" type="submit" display="hidden">Rechercher</button>
    </form>
    <form action="view_dossard_admin.php" method="post">
        <label for="nom">Entrer le nom:</label>
        <input type="nom" id="nom" name="nom" required>
                <br>
        <button class="submit" type="submit" display="hidden">Rechercher</button>
    </form>
<script>
    // Fonction pour changer la couleur du bouton en fonction du paramètre GET 'course'
            boutonChercher.style.backgroundColor = "#fbbd13"; // Changer la couleur en vert
</script>
</body>
</html>
