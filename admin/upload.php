<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <LINK rel="stylesheet" type="text/css" href="../style.css">
    <title>Formulaire d'upload des listes d'inscrits</title>
</head>
<body>
    <h2><center>Formulaire d'upload des listes d'inscrits</center></h2>
    <form action="upload.php" method="post" enctype="multipart/form-data">
        <input type="radio" id="open_file" name="file_type" value="open" checked>
        <label for="open_file">Uploader liste_inscrits_open</label><br>
        <input type="radio" id="course_file" name="file_type" value="course">
        <label for="course_file">Uploader liste_inscrits_course</label><br><br>
        <label for="file">Sélectionnez un fichier CSV :</label><br>
        <input type="file" id="file" name="file" accept=".csv"><br><br>
        <input type="submit" value="Envoyer">
    </form>
</body>
</html>

<?php
// Vérification si le formulaire a été soumis
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    // Vérification si le fichier a été uploadé sans erreur
    if (isset($_FILES["file"]) && $_FILES["file"]["error"] == 0) {
        $target_dir = "../";
        $file_type = $_POST["file_type"]; // Récupération de la valeur du bouton radio sélectionné
        
        // Détermination du nom du fichier cible en fonction du choix
        $target_file = ($file_type == "open") ? $target_dir . "liste_inscrits_open.csv" : $target_dir . "liste_inscrits_course.csv";
        
        $uploadOk = 1;
        $fileType = strtolower(pathinfo($_FILES["file"]["name"], PATHINFO_EXTENSION));
        
        // Vérification du format du fichier
        if($fileType != "csv" && $fileType != "csv") {
            echo "Seuls les fichiers CSV sont autorisés.";
            $uploadOk = 0;
        }
        
        // Vérification de la taille du fichier
        if ($_FILES["file"]["size"] > 5000000) {
            echo "Désolé, votre fichier est trop volumineux.";
            $uploadOk = 0;
        }
        
        // Si le fichier existe déjà, on le supprime
        if (file_exists($target_file)) {
            unlink($target_file); // Suppression du fichier existant
        }
        
        // Vérification si $uploadOk est défini à 0 par une erreur
        if ($uploadOk == 0) {
            echo "Désolé, votre fichier n'a pas été uploadé.";
        } else {
            // Si tout est ok, on tente d'uploader le fichier
            if (move_uploaded_file($_FILES["file"]["tmp_name"], $target_file)) {
                echo "Le fichier a été uploadé avec succès.";
            } else {
                echo "Une erreur s'est produite lors de l'upload de votre fichier.";
            }
        }
    } else {
        echo "Erreur : Aucun fichier n'a été uploadé.";
    }
}
?>

