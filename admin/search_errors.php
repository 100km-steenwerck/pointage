<?php require_once '../config.php';?>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <LINK rel="stylesheet" type="text/css" href="../style.css">
    <title>Formulaire</title>
</head>
<body>
<header>
<?php require_once('menu_admin.php');?>
</header>
<div class="container">
<table><?php
$p=0;

foreach ($dbname as $db) {

    echo "<tr><th>$table $db - $lieu[$p]</th><th>Anomalie</th>";
    
// Créer la connexion à la base de données
$conn = mysqli_connect($servername, $username, $password, $db);

// Vérifier la connexion
if ($conn->connect_error) {
    die("La connexion a échoué : " . $conn->connect_error);
    echo "La connexion a échoué";
}

    $sql = "SELECT Dossard, COUNT(*) as nombre_passages FROM `$table` GROUP BY Dossard HAVING nombre_passages > 3;";
    $result = $conn->query($sql);

    if (empty($result)) {
        echo "Aucune anomalie détectée à ce pointage.";
    } else {
	while($row = $result->fetch_assoc())
		{
		echo "<tr onclick=window.location='view_dossard_admin.php?dossard=" . $row["Dossard"] . "'><td>".$row["Dossard"]."</td><td>Trop de passages (>3)</td></tr>";
		}
        }

// Fermer la connexion
$conn->close();
$p++;
}
?>
</table>
</div>
</body>
</html>
