<?php require_once '../config.php';?>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <LINK rel="stylesheet" type="text/css" href="../style.css">
    <title>BDD Status</title>
</head>
<body>
<header>
<?php require_once('menu_admin.php');?>
</header>
<div class="container">
<table><?php

$masters = array("mariadb1","mariadb2","mariadb3","mariadb4","mariadb5");

foreach ($masters as $master) {

    echo "<tr><th>".$master."</th><td>";
    
// Créer la connexion à la base de données
$conn = mysqli_connect($servername, $username, $password, $db);

// Vérifier la connexion
if ($conn->connect_error) {
    die("La connexion a échoué : " . $conn->connect_error);
    echo "La connexion a échoué";
}

// Définir la connexion maître par défaut
$conn->query("SET @@default_master_connection='$master'");

$sql = "SHOW SLAVE STATUS;";
$result = $conn->query($sql);

if ($result->num_rows > 0) {
    // Affichage des données de réplication
    while($row = $result->fetch_assoc()) {
        echo "Slave_IO_State: " . $row["Slave_IO_State"]. "<br>";
        echo "Slave_SQL_Running: " . $row["Slave_SQL_Running"]. "<br>";
        echo "Last_IO_Error: " . $row["Last_IO_Error"]. "<br>";
        echo "Slave_SQL_Running_State:" . $row["Slave_SQL_Running_State"]. "<br>";
        // Afficher d'autres informations pertinentes si nécessaire
    }
} else {
    echo "Aucun résultat trouvé";
}
echo "</td>";
// Fermer la connexion
$conn->close();
}
?>
</table>
</div>
</body>
</html>
