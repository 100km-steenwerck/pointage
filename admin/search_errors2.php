<?php require_once '../config.php';?>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <LINK rel="stylesheet" type="text/css" href="../style.css">
    <title>Formulaire</title>
</head>
<body>
<header>
<?php require_once('menu_admin.php');?>
</header>
<div class="container">
<table><?php

    echo "<tr><th>Dossard</th><th>Anomalie</th>";
    
// Créer la connexion à la base de données
$conn = mysqli_connect($servername, $username, $password, $db);

// Vérifier la connexion
if ($conn->connect_error) {
    die("La connexion a échoué : " . $conn->connect_error);
    echo "La connexion a échoué";
}

    // Anomalie pointage manquant
    $sql = "
SELECT
	Dossard
FROM (
  SELECT
	Dossard,
    COALESCE(MAX(CASE WHEN Pointage = 1 THEN compteur END),0) AS Compteur_Pointage_1,
    COALESCE(MAX(CASE WHEN Pointage = 2 THEN compteur END),0) AS Compteur_Pointage_2,
    COALESCE(MAX(CASE WHEN Pointage = 3 THEN compteur END),0) AS Compteur_Pointage_3,
    COALESCE(MAX(CASE WHEN Pointage = 4 THEN compteur END),0) AS Compteur_Pointage_4,
    COALESCE(MAX(CASE WHEN Pointage = 5 THEN compteur END),0) AS Compteur_Pointage_5
FROM (
    SELECT
        ToutesLesBases.Dossard,
        ToutesLesBases.Pointage,
    	COUNT(*) as compteur
FROM (
    SELECT '1' AS Pointage, Pointage1.$table.* FROM Pointage1.$table
    UNION ALL
    SELECT '2' AS Pointage, Pointage2.$table.* FROM Pointage2.$table
    UNION ALL
    SELECT '3' AS Pointage, Pointage3.$table.* FROM Pointage3.$table
    UNION ALL
    SELECT '4' AS Pointage, Pointage4.$table.* FROM Pointage4.$table
    UNION ALL
    SELECT '5' AS Pointage, Pointage5.$table.* FROM Pointage5.$table
) AS ToutesLesBases
GROUP BY ToutesLesBases.Dossard, ToutesLesBases.Pointage ) AS requete
GROUP BY requete.Dossard
) AS newrequete
WHERE
    Compteur_Pointage_2 > Compteur_Pointage_1
    OR Compteur_Pointage_3 > Compteur_Pointage_2
    OR Compteur_Pointage_4 > Compteur_Pointage_3
    OR Compteur_Pointage_5 > Compteur_Pointage_4
    OR Compteur_Pointage_1 > (Compteur_Pointage_5 + 1)
    ";
    $result = $conn->query($sql);

    if (empty($result)) {
        echo "Aucune anomalie détectée à ce pointage.";
    } else {
	while($row = $result->fetch_assoc())
		{
		echo "<tr onclick=window.location='view_dossard_admin.php?dossard=" . $row["Dossard"] . "'><td>".$row["Dossard"]."</td><td>Passé à un pointage sans être passé par le précédent</td></tr>";
		}
        }

// Fermer la connexion
$conn->close();

?>
</table>
</div></body>
</html>
