#!/bin/python3
import csv

def csv_to_sql_insert(csv_file, table_name):
    with open(csv_file, 'r', encoding='utf-8') as file:
        csv_reader = csv.DictReader(file)

        for row in csv_reader:
            columns = ', '.join(row.keys())
            values = ', '.join([f"'{value}'" for value in row.values()])

            sql_insert = f"INSERT INTO `{table_name}` ({columns}) VALUES ({values});"
            print(sql_insert)

# Exemple d'utilisation
csv_file_path = 'liste.csv'
table_name = '2024'
csv_to_sql_insert(csv_file_path, table_name)
