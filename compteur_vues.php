<?php
// Chemin du fichier où sera stocké le nombre de vues
$filename_vues = 'compteur_vues.txt';
$filename_vues_unique = 'compteur_vues_unique.txt';

// Fonction pour lire le nombre de vues depuis le fichier
function getViews($filename) {
    if (file_exists($filename)) {
        return (int) file_get_contents($filename);
    } else {
        return 0;
    }
}

// Fonction pour mettre à jour le nombre de vues et l'écrire dans le fichier
function updateViews($filename) {
    $views = getViews($filename) + 1;
    file_put_contents($filename, $views);
    return $views;
}

// Mettre à jour le nombre de vues
session_start();
if (!isset($_SESSION['visited'])) {
    // Si la session n'est pas définie, incrémente le compteur de vues
    $_SESSION['visited'] = true;
    $nombreDeVuesUnique = updateViews($filename_vues_unique);
}
else{
    $nombreDeVuesUnique = getViews($filename_vues_unique);
}
$nombreDeVues = updateViews($filename_vues);

// Afficher le nombre de vues
echo "Nombre de vues uniques : " . $nombreDeVuesUnique;
echo " | Nombre de vues : " . $nombreDeVues;
?>
