<?php require_once 'config.php';?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <LINK rel="stylesheet" type="text/css" href="style.css"> 
    <title>Formulaire</title>
</head>
<body>
<h1><center><?php echo "$db - $lieu ($table)";?></center></h1>
    <form action="pointage.php" method="post">
        <label for="number">Entrer le numéro de dossard :</label>
	<input type="number" id="number" name="number" min="1" max=<?php echo $nbrDossard;?> required>
        <br>
        <button type="submit" class="submit">Envoyer</button>
    </form>
<form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>">
    <input type="hidden" name="supprimer_derniere_entree" value="true">
    <input type="submit" class="submit" value="Supprimer la dernière entrée">
</form>
<script>document.getElementById('number').focus();</script>
<?php
$date = date('Y-m-d H:i:s');

// Créer la connexion à la base de données
$conn = mysqli_connect($servername, $username, $password, $db);

// Vérifier la connexion
if ($conn->connect_error) {
    die("La connexion a échoué : " . $conn->connect_error);
}

if (isset($_POST["supprimer_entree"])) {
    // Requête SQL pour supprimer la dernière entrée
    $supprimerDate = $_POST["supprimer_entree"];
    $sql = "DELETE FROM `$table` WHERE `Dossard` = $supprimerDate ORDER BY `Date` DESC LIMIT 1" ;
    $conn->query($sql);
}
if (isset($_POST["supprimer_derniere_entree"])) {
    // Requête SQL pour supprimer la dernière entrée
    $sql = "DELETE FROM `$table` ORDER BY `Date` DESC LIMIT 1";
    $conn->query($sql);
}
if (isset($_POST["number"])) {
// Récupérer les valeurs du formulaire
$dossard = $_POST['number'];

// Vérifier si la valeur est dans la plage autorisée
if ($dossard >= 1 && $dossard <= $nbrDossard) {
    // Enregistrer la valeur et le choix dans la base de données
    $sql = "INSERT INTO `$table` (`Dossard`, `Date`) VALUES ('$dossard','$date');";
    //$conn->query($sql);

    if ($conn->query($sql) === TRUE) {
        ;
    } else {
        echo "Erreur : " . $sql . "<br>" . $conn->error;
    }
} else {
    echo "Veuillez entrer un nombre entre 1 et 1200.";
}
$sql_vitesse = "
SELECT
Dossard,
MAX(Date) as Date,
MIN(Date) as Date_Precedente,
33 / TIME_TO_SEC(TIMEDIFF(MAX(Date), MIN(Date))) * 3600 AS vitesse_moyenne
FROM (SELECT
*
FROM $db.$table  WHERE Dossard = $dossard ORDER BY Date DESC LIMIT 2) as newrequete";
$result = $conn->query($sql_vitesse);
while ($row = $result->fetch_assoc()) {
	echo "<table><tr><td>Ajout du dossard " . $row["Dossard"] . " à " . $row["Date"] . "</td>";
		
		
	if ($row["Date_Precedente"]==$row["Date"]) {
	echo "<td>Premier passage</td>";
	}
	else {
	echo "<td>Dernier passage à " . $row["Date_Precedente"] . " pour une vitesse de " . $row["vitesse_moyenne"] . " km/h</td>";
	}
	if ($row["vitesse_moyenne"]>16){echo "<td bgcolor='red'>Erreur probable</td>";}
	echo "</tr></table>";
    }

}
$sql_select = "SELECT * FROM `$table` ORDER BY `Date` DESC LIMIT 20";
$result = $conn->query($sql_select);
echo "<table><th>Dossard</th><th>Date du dernier passage</th><th>Suppression</th>";
while ($row = $result->fetch_assoc()) {
	echo "<tr><td>" . $row["Dossard"] . "</td><td>" . $row["Date"] . "</td>";
	echo "<td><form method='post' id='supprimer' action='pointage.php'>
	    <input type='hidden' name='supprimer_entree' value='".$row["Dossard"]."'>
	    <input type='submit' class='submit' value='Supprimer'>
	</form></td>";
	echo "</tr>";
    }
echo "</table>";
// Fermer la connexion
$conn->close();
?>
</body>
<script>
// Sélectionnez tous les boutons de soumission de formulaire dans le document
var submitButtons = document.querySelectorAll('input[type="submit"]');

// Parcourez tous les boutons de soumission
submitButtons.forEach(function(button) {
    // Ajoutez un gestionnaire d'événements pour l'événement click sur chaque bouton
    button.addEventListener('click', function(event) {
        // Empêchez le comportement par défaut du bouton de soumission
        event.preventDefault();

        // Affichez une boîte de dialogue de confirmation
        if (confirm("Voulez-vous vraiment supprimer ce temps de passage ?")) {
            // Si l'utilisateur clique sur "OK", soumettez le formulaire
            button.closest('form').submit();
        } else {
            // Si l'utilisateur clique sur "Annuler", ne soumettez pas le formulaire
            console.log("Soumission du formulaire annulée");
        }
    });
});
</script>
</html>
